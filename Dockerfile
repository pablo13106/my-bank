# Stage 1: Build the application
FROM maven:3.8.4-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn package

# Stage 2: Create a lightweight image to run the application
FROM openjdk:17
WORKDIR /app
COPY --from=build /app/target/my-bank-0.0.1-SNAPSHOT.jar ./app.jar
EXPOSE 8080
CMD ["java", "-jar", "app.jar", "--server.address=0.0.0.0"]