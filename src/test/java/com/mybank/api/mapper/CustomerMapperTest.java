package com.mybank.api.mapper;

import com.mybank.account.model.Account;
import com.mybank.api.contracts.CustomerResponse;
import com.mybank.customer.model.Customer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CustomerMapperTest {

    @Test
    void testMapCustomer() {
        Customer customer = getCustomer();

        CustomerResponse customerResponse = CustomerMapper.mapCustomer(customer);

        assertNotNull(customerResponse);
        assertEquals("John", customerResponse.getName());
        assertEquals("Doe", customerResponse.getSurname());
        assertEquals(1500.0, customerResponse.getTotalBalance());

        assertEquals(2, customerResponse.getAccounts().size());
    }

    private static Customer getCustomer() {
        Customer customer = new Customer();
        customer.setName("John");
        customer.setSurname("Doe");

        Account account1 = Mockito.mock(Account.class);
        when(account1.getBalance()).thenReturn(1000.0);

        Account account2 = Mockito.mock(Account.class);
        when(account2.getBalance()).thenReturn(500.0);

        Set<Account> accounts = new HashSet<>();
        accounts.add(account1);
        accounts.add(account2);
        customer.setAccounts(accounts);
        return customer;
    }

}