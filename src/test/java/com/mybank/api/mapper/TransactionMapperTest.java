package com.mybank.api.mapper;

import com.mybank.api.contracts.TransactionResponse;
import com.mybank.transaction.model.Transaction;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TransactionMapperTest {

    @Test
    void testMapTransaction() {
        Transaction transaction = new Transaction();
        transaction.setDate(LocalDate.of(2023, 11, 1));
        transaction.setValue(100.0);

        TransactionResponse transactionResponse = TransactionMapper.mapTransaction(transaction);

        assertNotNull(transactionResponse);
        assertEquals(LocalDate.of(2023, 11, 1), transactionResponse.getDate());
        assertEquals(100.0, transactionResponse.getValue());
    }

}