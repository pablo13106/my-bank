package com.mybank.api.mapper;

import com.mybank.account.model.Account;
import com.mybank.api.contracts.AccountResponse;
import com.mybank.transaction.model.Transaction;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AccountMapperTest {

    @Test
    void testMapAccount() {

        Account account = getAccount();

        AccountResponse accountResponse = AccountMapper.mapAccount(account);

        assertNotNull(accountResponse);
        assertEquals("ACC123", accountResponse.getNumber());
        assertEquals(500.0, accountResponse.getBalance());

        assertEquals(2, accountResponse.getTransactions().size());
    }

    private static Account getAccount() {
        Account account = new Account();
        account.setAccountNumber("ACC123");

        Transaction transaction1 = new Transaction();
        transaction1.setId(1L);
        transaction1.setValue(200.0);
        Transaction transaction2 = new Transaction();
        transaction2.setId(2L);
        transaction2.setValue(300.0);

        Set<Transaction> transactions = new HashSet<>();
        transactions.add(transaction1);
        transactions.add(transaction2);
        account.setTransactions(transactions);
        return account;
    }
}