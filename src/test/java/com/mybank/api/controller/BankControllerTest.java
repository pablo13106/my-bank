package com.mybank.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybank.account.model.Account;
import com.mybank.account.service.AccountService;
import com.mybank.api.contracts.AccountRequest;
import com.mybank.customer.model.Customer;
import com.mybank.customer.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class BankControllerTest {

    @Mock
    private AccountService accountService;

    @Mock
    private CustomerService customerService;

    private BankController bankController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        bankController = new BankController(accountService, customerService);
    }

    @Test
    void testCreateAccount() throws Exception {
        long customerId = 1L;
        double initialCredit = 100.0;

        AccountRequest accountRequest = new AccountRequest(customerId, initialCredit);

        Customer customer = new Customer();
        customer.setId(customerId);

        Account account = new Account();
        account.setId(1L);

        when(customerService.getCustomerById(customerId)).thenReturn(customer);
        when(accountService.createAccountForCustomer(customer, initialCredit)).thenReturn(account);

        mockMvc = MockMvcBuilders.standaloneSetup(bankController).build();

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/bank/accounts")
                        .content(asJsonString(accountRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void testGetCustomerInformation() throws Exception {
        long customerId = 1L;

        Customer customer = new Customer();
        customer.setId(customerId);

        when(customerService.getCustomerWithAccounts(customerId)).thenReturn(customer);

        mockMvc = MockMvcBuilders.standaloneSetup(bankController).build();

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/bank/customers/{customerId}", customerId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}