package com.mybank.transaction.service;

import com.mybank.account.model.Account;
import com.mybank.transaction.model.Transaction;
import com.mybank.transaction.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionServiceImplTest {

    @Mock
    private TransactionRepository transactionRepository;

    private TransactionServiceImpl transactionService;

    @BeforeEach
    void setUp() {
        transactionService = new TransactionServiceImpl(transactionRepository);
    }

    @Test
    void testCreateTransaction() {
        Account account = new Account();
        double initialCredit = 100.0;

        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setValue(initialCredit);
        transaction.setDate(LocalDate.now());

        when(transactionRepository.save(Mockito.any(Transaction.class))).thenReturn(transaction);

        Transaction createdTransaction = transactionService.createTransaction(account, initialCredit);

        verify(transactionRepository, Mockito.times(1)).save(Mockito.any(Transaction.class));

        assertNotNull(createdTransaction);
        assertEquals(account, createdTransaction.getAccount());
        assertEquals(initialCredit, createdTransaction.getValue());
        assertEquals(LocalDate.now(), createdTransaction.getDate());
    }

    @Test
    void testGetTransactionsForAccount() {
        Account account = new Account();
        Transaction transaction1 = new Transaction();
        Transaction transaction2 = new Transaction();
        Set<Transaction> transactions = new HashSet<>();
        transactions.add(transaction1);
        transactions.add(transaction2);

        when(transactionRepository.findByAccount(account)).thenReturn(transactions);

        Set<Transaction> retrievedTransactions = transactionService.getTransactionsForAccount(account);

        assertNotNull(retrievedTransactions);
        assertEquals(transactions.size(), retrievedTransactions.size());
    }

}