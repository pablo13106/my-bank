package com.mybank.account.service;

import com.mybank.account.accountnumber.AccountNumberGenerationService;
import com.mybank.account.model.Account;
import com.mybank.account.repository.AccountRepository;
import com.mybank.customer.model.Customer;
import com.mybank.transaction.service.TransactionService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {
    @Mock
    private AccountRepository accountRepository;

    @Mock
    private TransactionService transactionService;

    @Mock
    private AccountNumberGenerationService accountNumberGenerationService;

    private AccountServiceImpl accountService;

    @BeforeEach
    void setUp() {
        accountService = new AccountServiceImpl(accountRepository, transactionService, accountNumberGenerationService);
    }

    @Test
    void testCreateAccountForCustomer() {
        Customer customer = new Customer();
        double initialCredit = 100.0;

        when(accountNumberGenerationService.generateNewAccountNumber()).thenReturn("ACC123");
        when(accountRepository.save(Mockito.any(Account.class))).thenReturn(new Account());

        Account account = accountService.createAccountForCustomer(customer, initialCredit);

        verify(accountRepository, Mockito.times(1)).save(Mockito.any(Account.class));
        verify(transactionService, Mockito.times(1)).createTransaction(Mockito.any(Account.class), Mockito.eq(initialCredit));

        assertNotNull(account);
        assertEquals("ACC123", account.getAccountNumber());
        assertEquals(customer, account.getCustomer());
        assertEquals(1, account.getTransactions().size());
    }

    @Test
    void testGetAccountById() {
        long accountId = 1L;
        Account account = new Account();
        account.setId(accountId);

        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));

        Account retrievedAccount = accountService.getAccountById(accountId);

        assertNotNull(retrievedAccount);
        assertEquals(accountId, retrievedAccount.getId());
    }

    @Test
    void testGetAccountByIdNotFound() {
        long accountId = 1L;

        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> accountService.getAccountById(accountId));
    }
}