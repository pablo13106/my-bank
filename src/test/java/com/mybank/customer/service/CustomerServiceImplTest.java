package com.mybank.customer.service;

import com.mybank.account.model.Account;
import com.mybank.account.service.AccountService;
import com.mybank.customer.model.Customer;
import com.mybank.customer.repository.CustomerRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AccountService accountService;

    @BeforeEach
    public void setUp() {
        customerService = new CustomerServiceImpl(customerRepository, accountService);
    }

    @Test
    void testGetCustomerById() {
        long customerId = 1L;
        Customer customer = new Customer();
        customer.setId(customerId);

        Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));

        Customer retrievedCustomer = customerService.getCustomerById(customerId);

        assertNotNull(retrievedCustomer);
        assertEquals(customerId, retrievedCustomer.getId());
    }

    @Test
    void testGetCustomerByIdNotFound() {
        long customerId = 1L;

        Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> customerService.getCustomerById(customerId));
    }

    @Test
    void testGetCustomerWithAccounts() {
        long customerId = 1L;
        Customer customer = new Customer();
        customer.setId(customerId);

        Set<Account> accounts = new HashSet<>();
        Account account1 = new Account();
        Account account2 = new Account();
        accounts.add(account1);
        accounts.add(account2);

        Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));
        Mockito.when(accountService.getAccountsForCustomer(customer)).thenReturn(accounts);

        Customer customerWithAccounts = customerService.getCustomerWithAccounts(customerId);

        assertNotNull(customerWithAccounts);
        assertEquals(customerId, customerWithAccounts.getId());
        assertEquals(accounts.size(), customerWithAccounts.getAccounts().size());
    }
}