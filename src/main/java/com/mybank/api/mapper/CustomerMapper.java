package com.mybank.api.mapper;

import com.mybank.api.contracts.CustomerResponse;
import com.mybank.customer.model.Customer;

import java.util.stream.Collectors;

public class CustomerMapper {

    private CustomerMapper() {}

    public static CustomerResponse mapCustomer(Customer customer) {
        return CustomerResponse.builder()
                .name(customer.getName())
                .surname(customer.getSurname())
                .accounts(customer.getAccounts()
                        .stream()
                        .map(AccountMapper::mapAccount)
                        .collect(Collectors.toSet())
                )
                .totalBalance(customer.getTotalBalance())
                .build();
    }
}
