package com.mybank.api.mapper;

import com.mybank.account.model.Account;
import com.mybank.api.contracts.AccountResponse;

import java.util.stream.Collectors;

public class AccountMapper {

    private AccountMapper() {}

    public static AccountResponse mapAccount(Account account) {
        return AccountResponse.builder()
                .number(account.getAccountNumber())
                .balance(account.getBalance())
                .transactions(account.getTransactions()
                        .stream()
                        .map(TransactionMapper::mapTransaction)
                        .collect(Collectors.toSet())
                )
                .build();
    }
}
