package com.mybank.api.mapper;

import com.mybank.api.contracts.TransactionResponse;
import com.mybank.transaction.model.Transaction;

public class TransactionMapper {

    private TransactionMapper() {}
    public static TransactionResponse mapTransaction(Transaction transaction) {
        return TransactionResponse.builder()
                .date(transaction.getDate())
                .value(transaction.getValue())
                .build();
    }
}
