package com.mybank.api.contracts;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class AccountResponse {
    private String number;
    private double balance;
    private Set<TransactionResponse> transactions;
}
