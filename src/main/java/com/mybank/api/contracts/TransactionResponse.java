package com.mybank.api.contracts;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Builder
@Data
public class TransactionResponse {
    private double value;
    private LocalDate date;
}
