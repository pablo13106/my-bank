package com.mybank.api.contracts;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class CustomerResponse {
    private String name;
    private String surname;

    private double totalBalance;

    private Set<AccountResponse> accounts;
}
