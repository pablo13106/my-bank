package com.mybank.api.controller;

import com.mybank.account.model.Account;
import com.mybank.account.service.AccountService;
import com.mybank.api.contracts.AccountRequest;
import com.mybank.api.contracts.AccountResponse;
import com.mybank.api.contracts.CustomerResponse;
import com.mybank.api.mapper.AccountMapper;
import com.mybank.api.mapper.CustomerMapper;
import com.mybank.customer.model.Customer;
import com.mybank.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bank")
public class BankController {

    private final AccountService accountService;
    private final CustomerService customerService;

    @Autowired
    public BankController(AccountService accountService, CustomerService customerService) {
        this.accountService = accountService;
        this.customerService = customerService;
    }

    @PostMapping(value = "/accounts", produces = "application/json")
    public ResponseEntity<AccountResponse> createAccount(@RequestBody AccountRequest request) {
        Customer customer = customerService.getCustomerById(request.getCustomerId());
        Account account = accountService.createAccountForCustomer(customer, request.getInitialCredit());
        return new ResponseEntity<>(AccountMapper.mapAccount(account), HttpStatus.CREATED);
    }

    @GetMapping(value = "/customers/{customerId}", produces = "application/json")
    public ResponseEntity<CustomerResponse> getCustomerInformation(@PathVariable long customerId) {
        Customer customer = customerService.getCustomerWithAccounts(customerId);
        return new ResponseEntity<>(CustomerMapper.mapCustomer(customer), HttpStatus.OK);
    }
}
