package com.mybank.account.service;

import com.mybank.account.model.Account;
import com.mybank.customer.model.Customer;

import java.util.Set;

public interface AccountService {
    Account createAccountForCustomer(Customer customer, double initialCredit);

    Account getAccountById(long accountId);

    Set<Account> getAccountsForCustomer(Customer customer);
}
