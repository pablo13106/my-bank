package com.mybank.account.service;

import com.mybank.account.accountnumber.AccountNumberGenerationService;
import com.mybank.account.model.Account;
import com.mybank.account.repository.AccountRepository;
import com.mybank.customer.model.Customer;
import com.mybank.transaction.model.Transaction;
import com.mybank.transaction.service.TransactionService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final TransactionService transactionService;
    private final AccountNumberGenerationService accountNumberGenerationService;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository,
                              TransactionService transactionService,
                              AccountNumberGenerationService accountNumberGenerationService) {
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
        this.accountNumberGenerationService = accountNumberGenerationService;
    }

    @Override
    public Account createAccountForCustomer(Customer customer, double initialCredit) {
        Account account = prepareAccount(customer);
        if (initialCredit != 0) {
            addInitialCredit(account, initialCredit);
        }
        return account;
    }

    @Override
    public Account getAccountById(long accountId) {
        return accountRepository.findById(accountId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Account with id %s not found", accountId)));

    }

    @Override
    public Set<Account> getAccountsForCustomer(Customer customer) {
        Set<Account> accounts = accountRepository.findAccountsByCustomer(customer);
        accounts.forEach(account -> account.setTransactions(transactionService.getTransactionsForAccount(account)));
        return accounts;
    }

    private Account prepareAccount(Customer customer) {
        Account account = new Account();
        account.setAccountNumber(accountNumberGenerationService.generateNewAccountNumber());
        account.setCustomer(customer);
        accountRepository.save(account);
        return account;
    }

    private void addInitialCredit(Account account, double initialCredit) {
        Transaction transaction = transactionService.createTransaction(account, initialCredit);
        account.getTransactions().add(transaction);
    }
}
