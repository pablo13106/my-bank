package com.mybank.account.repository;

import com.mybank.account.model.Account;
import com.mybank.customer.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Set<Account> findAccountsByCustomer(Customer customer);
}
