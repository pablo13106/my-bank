package com.mybank.account.accountnumber;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AccountNumberGeneratorImpl implements AccountNumberGenerator {

    private static final int LENGTH = 34;
    private static final Random random = new Random();

    @Override
    public String getAccountNumber() {
        StringBuilder sb = new StringBuilder(LENGTH);

        while (sb.length() < LENGTH) {
            char randomChar = generateRandomAlphanumericChar();
            sb.append(randomChar);
        }

        return sb.toString();
    }

    private char generateRandomAlphanumericChar() {
        char randomChar;
        do {
            randomChar = (char) (random.nextInt(127));
        } while (!Character.isLetterOrDigit(randomChar));

        return randomChar;
    }
}
