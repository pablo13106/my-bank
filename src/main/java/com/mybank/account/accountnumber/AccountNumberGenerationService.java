package com.mybank.account.accountnumber;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountNumberGenerationService {

    private final AccountNumberGenerator accountNumberGenerator;

    @Autowired
    public AccountNumberGenerationService(AccountNumberGenerator accountNumberGenerator) {
        this.accountNumberGenerator = accountNumberGenerator;
    }

    public String generateNewAccountNumber() {
        return accountNumberGenerator.getAccountNumber();
    }
}
