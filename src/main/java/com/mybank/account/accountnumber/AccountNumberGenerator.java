package com.mybank.account.accountnumber;

public interface AccountNumberGenerator {
    String getAccountNumber();
}
