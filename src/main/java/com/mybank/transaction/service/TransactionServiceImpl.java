package com.mybank.transaction.service;

import com.mybank.account.model.Account;
import com.mybank.transaction.model.Transaction;
import com.mybank.transaction.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Transaction createTransaction(Account account, double initialCredit) {
        return transactionRepository.save(prepareTransaction(account, initialCredit));
    }

    @Override
    public Set<Transaction> getTransactionsForAccount(Account account) {
        return transactionRepository.findByAccount(account);
    }

    private Transaction prepareTransaction(Account account, double initialCredit) {
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setValue(initialCredit);
        transaction.setDate(LocalDate.now());
        return transaction;
    }
}
