package com.mybank.transaction.service;

import com.mybank.account.model.Account;
import com.mybank.transaction.model.Transaction;

import java.util.Set;

public interface TransactionService {
    Transaction createTransaction(Account account, double initialCredit);

    Set<Transaction> getTransactionsForAccount(Account account);
}
