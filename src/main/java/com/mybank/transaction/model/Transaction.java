package com.mybank.transaction.model;

import com.mybank.account.model.Account;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "\"value\"")
    private double value;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
}
