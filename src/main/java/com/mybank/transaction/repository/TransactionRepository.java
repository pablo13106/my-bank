package com.mybank.transaction.repository;

import com.mybank.account.model.Account;
import com.mybank.transaction.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Set<Transaction> findByAccount(Account account);
}
