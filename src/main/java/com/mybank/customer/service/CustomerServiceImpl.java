package com.mybank.customer.service;

import com.mybank.account.model.Account;
import com.mybank.account.service.AccountService;
import com.mybank.customer.model.Customer;
import com.mybank.customer.repository.CustomerRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    private final AccountService accountService;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, AccountService accountService) {
        this.customerRepository = customerRepository;
        this.accountService = accountService;
    }

    @Override
    public Customer getCustomerById(long id) {
        return customerRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(String.format("Customer with id %s not found", id)));
    }

    @Override
    public Customer getCustomerWithAccounts(long id) {
        Customer customer = getCustomerById(id);
        Set<Account> accounts = accountService.getAccountsForCustomer(customer);
        customer.setAccounts(accounts);
        return customer;
    }
}
