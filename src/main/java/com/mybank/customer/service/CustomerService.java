package com.mybank.customer.service;

import com.mybank.customer.model.Customer;

public interface CustomerService {
    Customer getCustomerById(long id);

    Customer getCustomerWithAccounts(long id);
}
