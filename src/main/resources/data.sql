INSERT INTO customers (name, surname) VALUES ('John', 'Smith');
INSERT INTO customers (name, surname) VALUES ('Alice', 'Johnson');
INSERT INTO customers (name, surname) VALUES ('Michael', 'Brown');
INSERT INTO customers (name, surname) VALUES ('Emily', 'Davis');
INSERT INTO customers (name, surname) VALUES ('David', 'Lee');

INSERT INTO accounts (account_number, customer_id) VALUES ('A1B2C3', 1);
INSERT INTO accounts (account_number, customer_id) VALUES ('X9Y8Z7', 1);
INSERT INTO accounts (account_number, customer_id) VALUES ('P4Q5R6', 2);
INSERT INTO accounts (account_number, customer_id) VALUES ('G3H2I1', 3);
