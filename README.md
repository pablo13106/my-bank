# Running application:
````
docker-compose up
````

# Logging in:
````
user: user
password: userPass
````

# Application:
````
localhost:8080
````

# Swagger
````
http://localhost:8080/swagger-ui/index.html
````